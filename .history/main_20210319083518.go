package main

import (
	"errors"
	"math"
	"net/http"
	"strconv"
	"time"

	"github.com/bradfitz/slice"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Responce struct {
	Message string `json:"message"`
}

type User struct {
	Name     string  `json:"name"`
	Wallet   float64 `json:"wallet"`
	LoggedIn int64
}

const MINIMUM_CHARGE_AMOUNT = "0.01"

var loggedUsers map[string]*User

func init() {
	loggedUsers = make(map[string]*User)
}

func login(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)
	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	if username == "lucky" && password == "pass" {
		claims["name"] = "lucky"
	} else if username == "lucky2" && password == "pass" {
		claims["name"] = "lucky2"
	} else if username == "lucky3" && password == "pass" {
		claims["name"] = "lucky3"
	} else {
		return echo.ErrUnauthorized
	}

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return err
	}

	loggedUsers[username] = &User{
		Name:     username,
		Wallet:   20.0,
		LoggedIn: time.Now().Local().Unix(),
	}

	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}

func accessible(c echo.Context) error {
	return c.String(http.StatusOK, "Accessible")
}

func userStat(c echo.Context) error {
	res := make([]*User, 0)

	for key, _ := range loggedUsers {
		res = append(res, loggedUsers[key])
	}
	slice.Sort(res[:], func(i, j int) bool {
		return res[i].LoggedIn < res[j].LoggedIn
	})
	return c.JSON(http.StatusOK, res)
}

func chargeUser(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	username := claims["name"].(string)
	amount := c.QueryParam("amount")

	if len(amount) == 0 {
		amount = MINIMUM_CHARGE_AMOUNT
	}

	if user, ok := loggedUsers[username]; ok {

		parsedAmount, err := strconv.ParseFloat(amount, 64)
		if err != nil {
			return errors.New("The passed amount is not in proper format...")
		}

		if user.Wallet > parsedAmount {
			user.Wallet -= math.Round(parsedAmount*100) / 100
			return c.JSON(http.StatusOK, user)
		} else {
			return c.String(http.StatusForbidden, "Not enough amount in the wallet...")
		}

	}
	return c.String(http.StatusNotFound, "The user is not found...")
}

func bankruptUser(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	username := claims["name"].(string)

	if user, ok := loggedUsers[username]; ok {
		user.Wallet = 0.0
		return c.JSON(http.StatusOK, user)
	}
	return c.String(http.StatusNotFound, "The user is not found...")
}

func logOutUsers(c echo.Context) error {
	loggedUsers = make(map[string]*User)
	return c.JSON(http.StatusOK, "OK")
}

func restricted(c echo.Context) error {
	//user := c.Get("user").(*jwt.Token)
	//claims := user.Claims.(jwt.MapClaims)
	//name := claims["name"].(string)
	r := &Responce{
		Message: "ok",
	}
	return c.JSON(http.StatusOK, r)
}

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORS())

	// Login route
	e.POST("/login", login)
	e.GET("/stats", userStat)
	e.GET("/logout_users", logOutUsers)
	// Unauthenticated route
	e.GET("/", accessible)

	// Restricted group
	r := e.Group("/authorize")
	r.Use(middleware.JWT([]byte("secret")))
	r.GET("", restricted)
	r.GET("/charge", chargeUser)
	r.GET("/bankrupt", bankruptUser)
	e.Logger.Fatal(e.Start(":8080"))
}
