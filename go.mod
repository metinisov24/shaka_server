module shaka_server

go 1.16

require (
	github.com/bradfitz/slice v0.0.0-20180809154707-2b758aa73013
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/labstack/echo/v4 v4.2.1
	go4.org v0.0.0-20201209231011-d4a079459e60 // indirect
)
